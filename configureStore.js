import { createStore , applyMiddleware , compose} from 'redux';
import thunk from 'redux-thunk';
import promise from 'redux-promise';
// import createLogger from 'redux-logger';
// import rootReducer from './reducers/index';
import {createReducer} from 'redux-act';

import _ from 'lodash';
// import simpleActions from './actions/actions';

const manageAmountOfActiveProcesses = store => next => action => {
    const { dispatch } = store;
    const result = next(action);
    const actionType = _.get(action, 'type');

    if ( _.endsWith(actionType, '_REQUEST') ) {
        dispatch(simpleActions.addProcess());
    }

    if ( _.endsWith(actionType, '_OK') || _.endsWith(actionType, '_ERROR') ) {
        dispatch(simpleActions.removeProcess());
    }

    return result;
};

const middleware = [ thunk, promise, manageAmountOfActiveProcesses ];

export default function configureStore(initialState){
    // const logger = createLogger();
    const enhancer= compose(applyMiddleware(...middleware));
    return createStore(createReducer({}, {}), initialState, enhancer);
}
