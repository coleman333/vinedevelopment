import { Provider } from "react-redux";
import { createAppContainer } from "react-navigation";
import configureStore from './configureStore';
import Navigator from "./components/Navigator";
const store = configureStore();

let React = require('react');
let Component = require('react');
// import Component from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends React.Component<Props> {
  render() {
    const Nav = createAppContainer(Navigator);
    return (
        <Provider store={store}>
            {/*<CardNavigator/>*/}
            <Nav />
        </Provider>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});
