import * as React from 'react';
import { createStackNavigator,createDrawerNavigator} from 'react-navigation';
import Main from "./Main";
// import textCamera from './textCamera';
// import BarCodeScanner from './BarCodeScanner';
// import ItemInfoFromBase from './ItemInfoFromBase';
import { Text, View ,TouchableOpacity,Image} from 'react-native';
import settings from '../images/settings.png';



const Navigator = createStackNavigator({

    // QrScanner: { screen: QrScanner,
    //     navigationOptions:({ navigation }) =>({
    //         headerLeft:null,
    //         header: null,
    //         headerRight:(
    //             <Button style={{marginRight:10}}
    //                 onPress={() =>navigation.navigate('menuToScanGood')}
    //                 title="Info"
    //                 //color="#fff"
    //             />
    //         )
    //     })
    // },

    // BarCodeScanner: {screen: BarCodeScanner,
    //     navigationOptions:({navigation})=>({
    //         header:null
    //     })
    // },

    Main: {screen: Main,
        navigationOptions:({ navigation }) =>({
            // headerLeft: null,
            //  header: null,
            title: 'Главная',
            headerStyle: {
                backgroundColor: 'gray',

            },
            // headerTintColor: '#fff',
            headerTitleStyle: {
                marginLeft:'45%',
                // display:'flex',
                // flex:1,
                // flexDirection:'row',
                // justifyContent:'center',
                // alignItems:'center',
                fontWeight: 'bold',
            },
            headerRight:(
                <TouchableOpacity
                    onPress={()=>{ let {navigate} = navigation;
                        navigate('Settings');}}  >
                    <Image source={settings} style={{width: 40, height: 40}}/>
                </TouchableOpacity>
            ),
            headerRightContainerStyle:{width: 70,justifyContent:'center'}
        })
    },

}, {initialRouteName: 'Main',

});

export default Navigator;
